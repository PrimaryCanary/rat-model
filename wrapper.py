import mouse_learn
from sys import stdin
from importlib import reload

initial = True
pre_post = 0  # [0, 1, 2] represent [pre, post, both]
N = 0
chunk_size = 300
epochs = 8
if __name__ == "__main__":
    while True:
        if initial:
            initial = False
            return_tuple = mouse_learn.prepare_datasets(pre_post, N, chunk_size)
        for i in range(100):
            mouse_learn.model(*return_tuple, epochs)
        # print("Press enter to retrain the model, CTRL-C to exit")
        exit()
        stdin.readline()
        reload(mouse_learn)
