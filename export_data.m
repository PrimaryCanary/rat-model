%clear all;

%load('YUN_GRANT_DATASET_FULL_UPDATED_13-Nov-2019.mat');
delete(['dff' filesep '*.bin']);
for i = 1:31
    for j = 1:2
        for k = 1:3
            f = fopen(['dff' filesep 'mouse-' num2str(i) '_file-' num2str(j) '_sess-' num2str(k) '.bin'], 'a');
            fwrite(f, [data.mouse(i).susceptible_idx size(data.mouse(i).file(j).sess(k).dff)], 'uint64');
            fwrite(f, reshape(data.mouse(i).file(j).sess(k).dff, 1, []), 'double');
            fclose(f);
        end
    end
end