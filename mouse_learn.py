import tensorflow as tf
from math import ceil
import numpy as np
from os.path import getsize
from sys import stdin
from random import shuffle
from struct import iter_unpack
import pandas as pd


def is_empty(filename):
    if getsize(filename) <= 24:
        print(filename, "appears to be empty, skipping")
        return True
    else:
        return False


def generate_files(mice, f, sess):
    files = []
    # Generate list of filenames
    if f == 0:
        files = [
            f"data/mouse-{i+1}_file-1_sess-{k+1}.csv"
            for i in range(mice)
            for k in range(sess)
        ]
    elif f == 1:
        files = [
            f"data/mouse-{i+1}_file-2_sess-{k+1}.csv"
            for i in range(mice)
            for k in range(sess)
        ]
    elif f == 2:
        files = [
            f"data/mouse-{i+1}_file-{j+1}_sess-{k+1}.csv"
            for i in range(mice)
            for j in range(2)
            for k in range(sess)
        ]
    else:
        print("Invalid pre/post specifier")
        exit()

    # Purge empty files
    files = list(filter(lambda f: not is_empty(f), files))
    shuffle(files)
    return files, len(files)


def parse_file_listing(path):
    with open(path, "r") as f:
        files = [
            tuple(l[:-1].split(",")) for l in f.readlines() if l != "\n" and l[0] != "#"
        ]
    return files, len(files)


def prepare_datasets(pre_post, choose_n_col, chunk_size):
    # Filenames of data, number of rows and columns of matrix data, classes of mice
    filenames = []
    num_files, num_train_examples, num_test_examples = 0, 0, 0

    # Get files
    print(
        "Path to file listing (leave blank for automatic file searching and classification)"
    )
    # path = stdin.readline().strip()
    path = ""
    if path == "":
        num_mice, num_sess = 31, 3
        filenames, num_files = generate_files(num_mice, pre_post, num_sess)
        num_train_examples = int(num_files * 0.7)
        num_test_examples = num_files - num_train_examples
    else:
        files, num_files = parse_file_listing(path)
        train_files, test_files = [
            name for name, dataset, *_ in files if dataset == "train"
        ], [name for name, dataset, *_ in files if dataset == "test"]
        filenames = train_files + test_files
        num_train_examples, num_test_examples = len(train_files), len(test_files)

    # Sanity check; data is partitioned correctly
    np.testing.assert_equal(num_train_examples + num_test_examples, num_files)
    print(
        "num_train_examples:",
        num_train_examples,
        "\nnum_test_examples:",
        num_test_examples,
    )

    ids = np.zeros(num_files)
    for i, f in zip(range(num_files), filenames):
        with open(f, "r") as fi:
            ids[i] = int(fi.readline().strip())

    dataframes = [
        pd.read_csv(f, header=None, usecols=range(6000), skiprows=[0], dtype=np.float64)
        for f in filenames
    ]

    print("dataframes read")

    rows, cols = np.zeros(num_files), np.zeros(num_files)
    for i, df in zip(range(num_files), dataframes):
        # print('Reading file:', df)
        rows[i], cols[i] = df.shape

    # Sanity checks; ensure every id, row, and column is an integer and have the correct size
    # map(lambda i: np.testing.assert_equal(i == 0 or i == 1, True), ids)
    map(lambda x: np.testing.assert_equal(x.dtype, np.dtype("uint64")), rows, cols)
    map(lambda x: np.testing.assert_equal(x > 0, True), rows, cols)
    np.testing.assert_equal([len(ids), len(rows), len(cols)], [num_files] * 3)

    # Used for ensuring every example has the same size
    max_rows, max_cols = int(max(rows)), int(max(cols))

    matrices = np.zeros((num_files, max_rows, max_cols))
    for i in range(num_files):
        # print('Reading file:', filenames[i])
        r, c = dataframes[i].shape
        matrices[i, :r, :c] = dataframes[i].to_numpy()

    # Sanity checks; ensure every matrix has the correct shape and contains doubles
    map(lambda x: np.testing.assert_equal(x.shape, (max_rows, max_cols)), matrices)
    map(lambda x: np.testing.assert_equal(x.dtype, np.dtype("float64")), matrices)

    assert (
        max_cols % chunk_size == 0
    ), "Invalid chunk size. Ensure the maximum number of columns is divible by the chunk size."
    chunks = max_cols // chunk_size
    num_train_examples *= chunks
    num_test_examples *= chunks
    max_cols = chunk_size
    dup_ids = np.array([ids[i] for i in range(num_files) for dups in range(chunks)])
    dup_filenames = np.array(
        [filenames[i] for i in range(num_files) for dups in range(chunks)]
    )

    # Sanity check; ensure ids and filenames are duplicated correctly
    np.testing.assert_equal(len(ids) * chunks, len(dup_ids))
    np.testing.assert_equal(len(filenames) * chunks, len(dup_filenames))
    np.testing.assert_equal([ids[0]] * chunks, dup_ids[:chunks])
    np.testing.assert_equal([filenames[0]] * chunks, dup_filenames[:chunks])

    ids = dup_ids
    filenames = dup_filenames

    matrices = np.reshape(matrices, (num_files * chunks, max_rows, chunk_size, 1))

    # Sanity check; ensure correct number of examples
    np.testing.assert_equal(len(dup_ids), len(matrices))

    print("matrices created")

    train_dataset = tf.data.Dataset.from_tensor_slices(
        (matrices[:num_train_examples], ids[:num_train_examples])
    )
    test_dataset = tf.data.Dataset.from_tensor_slices(
        (matrices[num_train_examples:], ids[num_train_examples:])
    )
    np.testing.assert_equal(train_dataset.cardinality().numpy(), num_train_examples)
    np.testing.assert_equal(test_dataset.cardinality().numpy(), num_test_examples)

    max_val, min_val = np.amax(matrices), np.amin(matrices)
    abs_max_val = max_val if max_val > min_val * -1 else min_val * -1
    print(f"Max value: {max_val}\nMin value: {min_val}")

    def normalize(feature, label):
        feature /= abs_max_val
        return feature, label

    train_dataset = train_dataset.map(normalize).cache()
    test_dataset = test_dataset.map(normalize).cache()
    return (
        train_dataset,
        test_dataset,
        num_train_examples,
        num_test_examples,
        max_rows,
        max_cols,
        filenames,
        matrices,
        ids,
        chunks,
    )


def model(
    train,
    test,
    num_train_examples,
    num_test_examples,
    max_rows,
    max_cols,
    filenames,
    matrices,
    ids,
    chunks,
    epoch,
):
    BATCH_SIZE = 32
    model = tf.keras.Sequential(
        [
            # tf.keras.layers.Conv2D(8, (3, 3), padding='same', activation = tf.nn.relu, input_shape = (max_rows, max_cols, 1)),
            # tf.keras.layers.MaxPooling2D((2, 2), strides = 2),
            # tf.keras.layers.Conv2D(16, (3, 3), padding = 'same', activation = tf.nn.relu),
            # tf.keras.layers.MaxPooling2D((2, 2), strides = 2),
            # tf.keras.layers.Flatten(),
            # tf.keras.layers.Flatten(input_shape = (max_rows, max_cols, 1)),
            # tf.keras.layers.Dense(64, activation = tf.nn.relu, input_shape=(max_rows, max_cols, 1)),
            tf.keras.layers.Flatten(input_shape=(max_rows, max_cols, 1)),
            tf.keras.layers.Dense(32, activation=tf.nn.relu),
            tf.keras.layers.Dropout(0.5),
            tf.keras.layers.Dense(16, activation=tf.nn.relu),
            tf.keras.layers.Dense(2, activation=tf.nn.softmax),
        ]
    )

    model.compile(
        optimizer="adam",
        loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        # loss = tf.keras.losses.BinaryCrossentropy(from_logits = True),
        metrics=["accuracy"],
    )

    train = train.cache().repeat().shuffle(num_train_examples).batch(BATCH_SIZE)
    test = test.batch(BATCH_SIZE).cache()
    model.fit(
        train, epochs=epoch, steps_per_epoch=ceil(num_train_examples / BATCH_SIZE)
    )

    test_loss, test_accuracy = model.evaluate(
        test, steps=ceil(num_test_examples / BATCH_SIZE)
    )
    print("Accuracy on test dataset:", test_accuracy)

    i = 0
    predictions, labels, train_labels = (
        np.zeros((num_test_examples, 2)),
        np.zeros(num_test_examples),
        np.zeros(num_train_examples),
    )
    for test_data, test_labels in test:
        ibatch = i * BATCH_SIZE
        ibatch_offset = ibatch + BATCH_SIZE
        # predictions[ibatch: ibatch_offset] = model.predict(test_data.numpy())
        labels[ibatch:ibatch_offset] = test_labels.numpy()
        i += 1

    predictions = model.predict(test)
    print(
        "Number of correct predictions:",
        sum(map(lambda pred, label: np.argmax(pred) == label, predictions, labels)),
    )
    for i in range(num_test_examples):
        print(
            "File:",
            filenames[num_train_examples + i],
            "\tActual label:",
            ids[num_train_examples + i],
            "\tPredicted label:",
            np.argmax(predictions[i]),
        )
